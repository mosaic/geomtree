Welcome
====================================================

.. image:: https://anaconda.org/mosaic/geomtree/badges/version.svg
   :target: https://anaconda.org/mosaic/geomtree

.. image:: https://anaconda.org/mosaic/geomtree/badges/latest_release_date.svg
   :target: https://anaconda.org/mosaic/geomtree

.. image:: https://anaconda.org/mosaic/geomtree/badges/platforms.svg
   :target: https://anaconda.org/mosaic/geomtree

.. image:: https://anaconda.org/mosaic/geomtree/badges/license.svg
   :target: https://anaconda.org/mosaic/geomtree

.. image:: https://anaconda.org/mosaic/geomtree/badges/installer/conda.svg
   :target: https://conda.anaconda.org/geomtree

.. image:: https://anaconda.org/mosaic/geomtree/badges/downloads.svg
   :target: https://conda.anaconda.org/mosaic/geomtree

.. image:: https://gitlab.inria.fr/mosaic/work-in-progress/geomtree/badges/master/pipeline.svg
  :target: https://gitlab.inria.fr/mosaic/work-in-progress/geomtree/commits/master

.. image:: https://gitlab.inria.fr/mosaic/work-in-progress/geomtree/badges/master/coverage.svg
  :target: https://gitlab.inria.fr/mosaic/work-in-progress/geomtree/commits/master

Contents
*****************

.. toctree::
    :maxdepth: 2

    Examples <examples/examples>

Indices and tables
***********************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
