Examples
========

.. toctree::
   :maxdepth: 4

   affinetransform
   geomtree
   ellipsoiddistance
