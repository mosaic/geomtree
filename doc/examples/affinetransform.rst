How to use the AffineTransform & PointCloud classes ?
=====================================================

First of all, import the package and the ``vtk_display_actors`` function from the Mosaic package ``visu_core``.

.. code-block:: python

    from visu_core.vtk.display import vtk_display_actors as display
    from geomtree import *

Visualize affine transformations
********************************

As mentionned in the documentation of the method ``AffineTransform.sample_points(...)``, affine transformation can be interpreted as Gaussian distributions.
Moreover, the method ``AffineTransform.get_actor(...)`` returns an ellipsoid actor. The radius of the ellipsoid can be controlled in the parameters (cf. documentation of that method).
By default, the radius is set to cover 95% of the points generated with the Gaussian distribution.

.. code-block:: python

    ellipNumber = 15
    sampNumber = 10000

    actors=[]

    for i in range(ellipNumber):
        tf = random_transform()
        points = tf.sample_points(sampNumber)
        actors+=tf.get_actor(axis=True,color='glasbey_'+str(i))
        actors+=points.get_actor(color='glasbey_'+str(i))

    display(actors,background=(0.9,0.9,0.9))

Inverse transformation
**********************

The method ``AffineTransform.inverse`` returns the inverse affine transformation. In the following piece of code,
the three actors should be unit sphere centered at the origin (therefore surimposed).

.. code-block:: python

    tf = random_transform()

    itf = tf.inverse()

    tfitf = tf.compose(itf) # should be identity
    itftf = itf.compose(tf) # should be identity

    actors = []
    actors+=tfitf.get_actor(color=1,axis=True)
    actors+=itftf.get_actor(color=3,axis=True)
    actors+=AffineTransform().get_actor(color=2,axis=True)
    display(actors)

Find intermediate transform
***************************************

The method ``AffineTransform.intermediate_transform`` returns the transformation :math:`T_2` such that :math:`T_3 = T_2T_1`, provided :math:`T_3` and :math:`T_1`.

.. code-block:: python

    tf1 = random_transform()
    tf3 = random_transform()

    tf2 = tf3.intermediate_transform(tf1)

    tf = tf2.compose(tf1) # should be equal to tf3

    actors = []
    actors+=tf3.get_actor(color=1,axis=True)
    actors+=tf.get_actor(color=2,axis=True)
    display(actors)

Filtering the Point Cloud
*************************
Let us have a ``PointCloud`` obtained after sampling points from a Gaussian distribution. As claimed earlier, at the visualisation step,
the ellipsoid of the transformation should contain 95% of the points sampled. The ``mahalanobis_distance_filtering`` method returns a vector of boolean, testing for
each point whether they belong to the ellipsoid or not. When plotting, the parameter ``filter`` of the method ``get_actor`` can be used to differentiate those points.

.. code-block:: python

    alpha=0.95

    tf = random_transform()
    points = tf.sample_points(100)

    filter = points.mahalanobis_distance_filter(tf,alpha)

    print(np.sum(filter)/len(filter)) # should be close to alpha

    color={True : 'glasbey_1', False : 'glasbey_2'} #Blue for points inside the ellipsoid; Red otherwise.

    actors=[]
    actors+=tf.get_actor(axis=True,alpha=alpha)
    actors+=points.get_actor(filter=filter,color=color,glyph='sphere',scale=0.1)

    display(actors,background=(0.9,0.9,0.9))


Estimation of Gaussian parameters
*********************************

Given a ``PointCloud`` obtained after sampling points from a Gaussian distribution, the method ``PointCloud.estimate_gaussian`` returns an estimation
of the parameters of that distribution. You can check the correctness of the estimation via the following piece of code.

.. code-block:: python

    n=1000
    tf = random_transform()
    pts = tf.sample_points(n)

    tfe = pts.estimate_gaussian()

    actors = []
    actors+=tf.get_actor(color=0,axis=True) #ground truth, in blue
    actors+=pts.get_actor(color=0,glyph='sphere',scale=0.1)
    actors+=tfe.get_actor(color=1,axis=True) #estimation, in red

    display(actors,background=(0.9,0.9,0.9))

Intersection detection
**********************

Cf. documentation of the ``PointCloud.check_intersection`` method for complete details.

Using the Bayes Factor, identify the points that could be sampled from different Gaussians distributions.

.. code-block:: python

    n=1000

    tf1 = AffineTransform()

    tf2 = AffineTransform()
    tf2.set_parameters(np.array([1,5,8]),np.diag([1,2,3]))

    pt1 = tf1.sample_points(n)
    pt2 = tf2.sample_points(n)

    color={True : 'g', False : 'r'} # green for points at the intersection, red otherwise
    glyph={True : 'sphere', False : 'point'} #only points at the intersection will be displayed as spheres

    inter1 = pt1.intersection_filter(tf1,tf2)
    inter2 = pt2.intersection_filter(tf1,tf2)

    actors=[]
    actors+=pt1.get_actor(filter=inter1,color=color,glyph=glyph)
    actors+=pt2.get_actor(filter=inter2,color=color,glyph=glyph)
    actors+=tf1.get_actor(axis=True)
    actors+=tf2.get_actor(axis=True)
    display(actors,background=(0.9,0.9,0.9))

