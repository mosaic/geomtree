How to use the geometric distance between ellipsoids
=====================================================

..  code-block:: python

    from geomtree import *
    from visu_core.vtk.display import vtk_display_actors as display

    tf1 = random_transform()
    tf2 = random_transform()

    display(tf1.get_actor()+tf2.get_actor())

    print(geometric_distance(tf1,tf2,alpha=0.95,epsilon=1e-6,view=False))
    print('#-------------------#')

    tf1 = AffineTransform()
    tf1.set_parameters(translation= np.array([2.86820768,3.89143435,4.33672974]),linear=np.array([[-0.55153873,-0.16148895,-0.00088868],[-0.3521366,0.21171356,-0.01869735],[ 0.16406091, -0.08847516, -0.04311911]]))

    tf2 = AffineTransform()
    tf2.set_parameters(translation=np.array([3.76642489, 3.60659314, 5.1294022 ]),linear=np.array([[-0.60064044, -0.02037255,  0.02495534],[-0.02739907, -0.03648569, -0.40604146],[ 0.05979144, -0.22137366,  0.06462512]]))

    display(tf1.get_actor()+tf2.get_actor())

    print(geometric_distance(tf1,tf2,alpha=0.95,epsilon=1e-6,view=False))
    print('#-------------------#')

    tf1 = AffineTransform()
    tf2 = AffineTransform()
    tf2.set_parameters(np.array([1, 5, 8]), np.diag([1, 2, 3]))

    display(tf1.get_actor()+tf2.get_actor())

    print(geometric_distance(tf1,tf2,alpha=0.95,epsilon=1e-6,view=True))
    print('#-------------------#')
