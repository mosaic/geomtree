How to use GeomTree class ?
===========================

.. code-block:: python

    from geomtree import *
    from treex import *
    from treex.simulation import *
    from visu_core.vtk.display import vtk_display_actors as display

    gt = tree_to_geomtree(gen_random_tree(15))
    actors = gt.get_actor()
    display(actors,background=(0.9,0.9,0.9))

