=======
Credits
=======

Development Lead
----------------

.. {# pkglts, doc.authors

* Florian Ingels, <florian.ingels@inria.fr>

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* Florian Ingels <florian.ingels@inria.fr>

.. #}
