========================
GeomTree
========================

.. {# pkglts, doc

.. #}

A package for treating trees augmented with 3D geometry.

:Development: Florian Ingels
:Contributors: Romain Azaïs, Guillaume Cerutti
:Language: Python 3
:Supported OS: Windows*, MacOS, Linux*
:Licence: CeCILL-C
:Documentation: https://mosaic.gitlabpages.inria.fr/geomtree/


Installation procedure
----------------------

Either way, you need to have Miniconda installed

As User
*******

It is better, due to compatibility issue, to create a new environment from scratch for ``geomtree`` :

::

   conda create -n geomtree -c mosaic geomtree

Activate the environment : 
::

   source activate geomtree

As Developer
************

First of all, clone the repo; then place yourself in the appropriate folder:

::

    cd path/to/geomtree
    
Create a new environment for ``geomtree`` :
  
::

    conda env create -n geomtree -f conda/env.yaml

Activate the environment : 

::

    conda activate geomtree

Install ``geomtree`` :   

::

   python setup.py develop
