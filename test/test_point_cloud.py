import unittest
import numpy as np
from geomtree.affine_transform import random_transform, AffineTransform
from geomtree.point_cloud import *
import os
from random import seed

class TestPointCloud(unittest.TestCase):

    seed(1234)

    def setUp(self):
        """
        Define class members that will be used for all tests
        """
        pass

    def tearDown(self):
        """
        Cleanup after the execution of every test method
        """
        pass

    def test_mahalanobis_distance_filter(self):
        alpha = 0.95
        n = 10**4

        tf = random_transform()
        pts = tf.sample_points(n)
        filter = pts.mahalanobis_distance_filter(tf, alpha)

        assert np.allclose(np.sum(filter) / len(filter),alpha,atol=9e-3)

    def test_estimate_gaussian(self):
        n = 10**7
        tf = random_transform()
        pts = tf.sample_points(n)
        tfe = pts.estimate_gaussian()

        assert np.linalg.norm(tf.translation - tfe.translation)<1e-3
        assert np.linalg.norm(tf.get_covariance_matrix()- tfe.get_covariance_matrix())<1e-3

    def test_intersection_filter(self):
        n = 1000
        tf1 = AffineTransform()
        tf2 = AffineTransform()
        tf2.set_parameters(np.array([1, 5, 7]), np.diag([1, 2, 3]))

        pt1 = tf1.sample_points(n)
        pt2 = tf2.sample_points(n)


        inter1 = pt1.intersection_filter(tf1,tf2)
        inter2 = pt2.intersection_filter(tf1,tf2)

        assert len(np.unique(inter1))==2
        assert len(np.unique(inter2))==2

        subcloud = pt1.subdivise_cloud(inter1)
        assert len(subcloud[False])+len(subcloud[True])==n