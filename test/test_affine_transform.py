import unittest
import numpy as np
from geomtree.affine_transform import *
import os
from random import seed

class TestAffineTransform(unittest.TestCase):

    seed(1234)

    def setUp(self):
        self.linear = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
        self.translation = np.array([1, 2, 3])
        self.tf1 = random_transform()
        self.tf3 = random_transform()
        self.data_dir = 'tmp/test/'
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)

    def tearDown(self):
        for file in os.listdir(self.data_dir):
            os.remove(self.data_dir+file)
        os.removedirs(self.data_dir)

    def test_id(self):
        tf = AffineTransform('test_id')
        assert tf.my_id=='test_id'

    def test_copy(self):
        tf = AffineTransform()
        tf.set_parameters(translation=self.translation, linear=self.linear)
        self.translation[1]=0
        assert tf.translation[1]==2
        self.linear[1,2]=0
        assert tf.linear[1,2]==6

        tfp = tf.copy()
        assert np.allclose(tf.translation,tfp.translation)
        assert np.allclose(tf.linear,tfp.linear)

    def test_inverse(self):
        itf = self.tf1.inverse()

        tfitf = self.tf1.compose(itf)
        itftf = itf.compose(self.tf1)

        assert np.allclose(tfitf.translation, np.array([0,0,0]))
        assert np.allclose(itftf.translation, np.array([0, 0, 0]))
        assert np.allclose(tfitf.linear, np.eye(3))
        assert np.allclose(itftf.linear, np.eye(3))

    def test_intermediate_transform(self):
        tf2 = self.tf3.intermediate_transform(self.tf1)

        tf = tf2.compose(self.tf1)

        assert np.allclose(tf.translation, self.tf3.translation)
        assert np.allclose(tf.linear, self.tf3.linear)

    def test_random_transform(self):
        tf1 = random_transform(translation=False)
        assert np.allclose(tf1.translation,np.array([0,0,0]))
        tf2 = random_transform(scale=False)
        assert np.allclose(np.linalg.det(tf2.linear),1)
        tf3 = random_transform(rotation=False)
        i, j = np.nonzero(tf3.linear)
        assert np.all(i == j)

    def test_hellinger_distance(self):
        tf1= random_transform()
        tf2 = AffineTransform()
        tf2.set_parameters(translation=self.translation, linear=self.linear)
        assert np.allclose(0,hellinger_distance(tf1,tf1))
        assert np.allclose(1,hellinger_distance(tf1,tf2))