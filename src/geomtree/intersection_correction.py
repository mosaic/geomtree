from scipy.spatial.transform import Rotation as R
import numpy as np
from numpy.linalg import norm


def normal_vector(x, y, z):
    vec = np.cross(x - z, y - z)
    return vec / norm(vec)


def rotation_angle(x, y, z):
    # z is the anchor
    # al-kashi
    a = norm(x - z)
    b = norm(y - z)
    c = norm(x - y)
    return np.arccos((a ** 2 + b ** 2 - c ** 2) / (2 * a * b))

def correct_intersection(tu,tv):
    _, x, y = geometric_distance(tu, tv, output=True)

    # en attendant les ancres
    z1 = -2*tu.translation
    z2 = 2*tv.translation

    m = 0.5*(x+y)

    n1 = normal_vector(x, y, z1)
    theta1 = -rotation_angle(x, m, z1)

    rot1 = R.from_rotvec(theta1 * n1)

    tfu = AffineTransform()
    tfu.set_parameters(linear=rot1.as_matrix())

    n2 = normal_vector(x, y, z2)
    theta2 = rotation_angle(m, y, z2)

    rot2 = R.from_rotvec(theta2 * n2)

    tfv = AffineTransform()
    tfv.set_parameters(linear=rot2.as_matrix())

    return  tfu.compose(tu), tfv.compose(tv)


idu,idv = detected[0]

u = gtp.subtree(node=idu)
v = gtp.subtree(node=idv)

tu = u.my_geometry
tv= v.my_geometry

tfu,tfv = correct_intersection(tu,tv)

display(tu.get_actor(color='r')+tv.get_actor(color='b'))

display(tfu.get_actor(color='r')+tfv.get_actor(color='b'))