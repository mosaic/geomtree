import numpy as np
from random import random
from copy import copy

from scipy.spatial.transform import Rotation as R
from scipy.linalg import svd, inv, det, norm
from scipy.stats import chi2

import itertools

class AffineTransform(object):
    """
    This class implements 3D affine transformations of the form :math:`Y = TX +t` where :math:`t` is a (3,) translation vector
    and :math:`T` is a (3,3) linear transformation matrix.

    :ivar my_id: int or str
    :ivar translation: (3,) array
    :ivar linear: (3,3) array

    """
    __new_id_iter__ = itertools.count()

    __slots__  = 'my_id', 'translation', 'linear'
    def __init__(self,my_id=None):
        """
        :param my_id: if provided, the name to give to the transform. If not, a unique id will be generated.
        """
        self.my_id = my_id if my_id is not None else next(AffineTransform.__new_id_iter__) #: Unique id attributed to the transformation
        self.translation = np.array([0,0,0]) #:Corresponds to the translation part :math:`t`.
        self.linear = np.eye(3) #: Corresponds to the linear transformation part :math:`T`.

    def copy(self):
        """
        Creates a deep copy of the transformation. Change the id.
        """
        tf = AffineTransform()
        tf.set_parameters(self.translation, self.linear)
        return tf

    def __str__(self):
        return "Translation : "+str(self.translation)+"\nLinear transformation :\n"+str(self.linear)

    def set_parameters(self, translation=None, linear=None):
        """
        Update parameters of the transformation, using copies of the arrays provided. Both are optional.

        :param translation: (3,) array, optional
        :param linear: (3,3) array, optional
        """
        if translation is not None:
            assert np.shape(translation)==(3,), 'translation must be (3,) numpy array'
            self.translation = copy(translation)
        if linear is not None:
            assert np.shape(linear)==(3,3), 'linear part must be (3,3) numpy array'
            self.linear = copy(linear)

    def apply_transform(self,data):
        """
        Given stack of vectors :math:`X_1,\dots,X_n`, returns :math:`Y_1,\dots,Y_n` where :math:`Y_i=TX_i+t`.

        :param data: (n,3) array

        :return: (n,3) array
        """
        T = self.linear
        pts = T @ data.T
        return pts.T + self.translation

    def get_covariance_matrix(self):
        """
        Denoting :math:`T` the linear part of the transformation, returns :math:`TT^{\\top}`.
        """
        T = self.linear
        return T @ T.T

    def sample_points(self,n=100,method='gaussian'):
        """
        Generate :math:`n` samples from the 3D normal law :math:`N(0,I_3)` and apply the transformation to them.

        The points obtained follow the gaussian distribution :math:`N(t, TT^{\\top})`.

        #TODO: doc with uniform sampling -> mvee

        :param n: int, optional (default 100)
        :return: PointCloud
        """
        from geomtree.point_cloud import PointCloud

        vec=np.random.multivariate_normal([0, 0, 0], np.eye(3), n)
        if method=='uniform':
            r = np.random.rand(n) ** (1. / 3)
            norm = np.linalg.norm(vec, axis=1)
            vec = (r / norm).reshape(-1, 1) * vec
        return PointCloud(self.apply_transform(vec))

    def get_actor(self,axis=False, box=False, opacity=0.2, alpha=0.95, color='r'):
        from visu_core.vtk.primitive_actors import ellipsoid_primitive, affine_frame_primitive, segment_primitive
        from visu_core.matplotlib.colormap import plain_colormap
        from visu_core.vtk.actor import vtk_actor
        from visu_core.vtk.polydata import face_scalar_property_polydata

        #TODO : doc
        qr = np.sqrt(chi2.ppf(alpha, 3))
        T = self.linear
        u,s,_ = svd(T @ T.T)
        tensor = u @ np.diag(np.sqrt(s)) @ u.T
        actors = [ellipsoid_primitive(self.translation,tensor, opacity=opacity, scale=qr, color=color)]
        if axis==True:
            actors+=[affine_frame_primitive(origin=self.translation, axes=qr * self.linear.T, lw=5)]
        if box==True:
            points = qr * (self.apply_transform(np.array([[-1, 1, -1], [1, 1, -1], [1, -1, -1], [-1, -1, -1],
                                                        [-1, 1, 1], [1, 1, 1], [1, -1, 1],
                                                        [-1, -1, 1]])) - self.translation) + self.translation

            actors += [segment_primitive(points[0],points[1],color=color),segment_primitive(points[0],points[4],color=color),
                       segment_primitive(points[0],points[3],color=color),segment_primitive(points[1],points[5],color=color),
                       segment_primitive(points[1],points[2],color=color),segment_primitive(points[3],points[2],color=color),
                       segment_primitive(points[3],points[7],color=color),segment_primitive(points[2],points[6],color=color),
                       segment_primitive(points[4],points[5],color=color),segment_primitive(points[4],points[7],color=color),
                       segment_primitive(points[7],points[6],color=color),segment_primitive(points[6],points[5],color=color)]

            colormap = plain_colormap(color)
            assert colormap is not None, 'color argument should be a valid color'
            point_polydata = face_scalar_property_polydata(points,
                                                           [[0, 1, 2, 3], [4, 5, 6, 7], [0, 3, 7, 4], [0, 1, 5, 4],
                                                            [2, 3, 7, 6], [1, 2, 6, 5]], np.zeros(6))
            actors += [vtk_actor(point_polydata, colormap=colormap, opacity=opacity, value_range=(0, 255))]

        return actors

    def inverse(self):
        """
        Return the inverse transformation, with linear transformation part :math:`T^{-1}` and translation part :math:`-T^{-1}t`.

        :return: AffineTransform
        """
        invT = inv(self.linear)
        translation = - invT @ self.translation
        ntf = AffineTransform()
        ntf.set_parameters(translation,invT)
        return ntf

    def volume(self):
        return det(self.linear)

    def compose(self,tf):
        """
        Given two affine transformations ``tf1`` and ``tf2``, ``tf2.compose(tf1)`` returns the transformation corresponding to applying first ``tf1`` and then ``tf2``.
        In other words, applied to :math:`X`, :math:`Y = t_2 + T_2(t_1+T_1X)`.

        Linear transformation part is then :math:`T_2 T_1` and translation part is :math:`t_2 + T_2t_1`.

        :param tf: AffineTransform
        :return: AffineTransform
        """
        ### return self * tf
        T1 = tf.linear
        T2 = self.linear
        t2 = self.translation
        t1 = tf.translation
        translation = t2+ T2 @ t1
        T = T2 @ T1
        ntf = AffineTransform()
        ntf.set_parameters(translation,T)
        return ntf

    def intermediate_transform(self, tf):
        r"""
        Given two affine transformations ``tf1`` and ``tf2``, ``tf2.intermediate_transform(tf1)`` returns the transformation ``tf3`` such that ``tf2 = tf3.compose(tf1)``.

        With a matrix formulation, ``tf3`` is the solution of the following system:

        .. math::
                Y &= t_2 + T_2X \\
                Y &= t_3 + T_3(t_1 + T1X)

        Which gives :math:`t_3 = t_2 - T_2T_1^{-1}t_1` and :math:`T_3 = T_2T_1^{-1}`.

        Alternatively, ``tf3`` is given by ``tf2.compose(tf1.inverse())``.

        :param tf: AffineTransform
        :return: AffineTransform

        See Also
        --------
        compose(...), inverse(...)
        """
        # return tfp such that self = tfp * tf
        return self.compose(tf.inverse())

#-----------------------------------------------------#

def random_transform(translation=True,scale=True,rotation=True):
    #translation = [(-1)**randint(0,1) for i in range(3)]*np.array([random(),random(),random()])*10
    if translation:
        t = np.array([random(),random(),random()])*10
    else:
        t = np.array([0,0,0])
    if scale:
        s = np.array([random(), random(), random()])
    else:
        s = np.array([1,1,1])
    if rotation:
        rot = R.random()
    else:
        rot = R.from_matrix(np.eye(3))

    tf = AffineTransform()
    tf.set_parameters(t,rot.as_matrix() @ np.diag(s))
    return tf

#-----------------------------------------------------#

def hellinger_distance(tf1,tf2,epsilon=1e-6): ### tester VS monte-carlo ?
    """
    See https://en.wikipedia.org/wiki/Hellinger_distance
    :param tf1: AffineTransform
    :param tf2: AffineTransform
    :return: float in [0,1]
    """
    t1 = tf1.translation
    t2 = tf2.translation
    sig1 = tf1.get_covariance_matrix()
    sig2 = tf2.get_covariance_matrix()
    sigm = (sig1+sig2)/2

    val = 1 - (det(sig1)*det(sig2))**(1/4) / det(sigm)**(1/2)*np.exp((-1/8) * np.transpose(t1 - t2) @ inv(sigm) @ (t1 - t2))
    if np.abs(val)<epsilon:
        val=0
    return np.sqrt(val)

#-----------------------------------------------------#

def belongs(tf,x,alpha=0.95):
    T = inv(tf.linear)
    b = T @ tf.translation
    return norm(T @ x -b)#<np.sqrt(chi2.ppf(alpha, 3))

def geometric_distance(tf1,tf2,alpha=0.95,epsilon=1e-6,iterations_max=float('Inf'),output=False,view=False): #wip
    # Algo from "On the distance between two ellipsoids" by A. Lin and S.-P. Han

    import sys
    import time

    def coeff(tf, c1, c2, alpha):
        T = inv(tf.linear)
        b = T @ tf.translation
        a = T @ (c2 - c1)
        beta = T @ c1 - b
        return [a @ a, 2 * a @ beta, beta @ beta - chi2.ppf(alpha, 3)]

    def angle(u, v):
        c = (u @ v) / (norm(u) * norm(v))
        return np.arccos(np.clip(c, -1, 1))

    def quadratic(tf, x):
        T = inv(tf.linear)
        b = T @ tf.translation
        A = T.transpose() @ T
        beta = -T.transpose() @ b
        return A @ x + beta

    if view:
        from visu_core.vtk.display import vtk_display_actors as display
        from visu_core.vtk.primitive_actors import segment_primitive
        import matplotlib.pyplot as plt
        from geomtree.point_cloud import PointCloud

    ### Initialisation
    c1 = tf1.translation
    c2 = tf2.translation

    ### Main step
    j=0
    while j<iterations_max:
        s1 = [i for i in np.roots(coeff(tf1, c1, c2,alpha)) if 0 <= i <= 1]
        s2 = [i for i in np.roots(coeff(tf2, c1, c2,alpha)) if 0 <= i <= 1]

        t1 = max(s1) if len(s1)>0 else 1
        t2 = min(s2) if len(s2)>0 else 0

        xbar = c1 + t1 * (c2 - c1)
        ybar = c1 + t2 * (c2 - c1)

        if t2<t1:
            #print('\n')
            if output:
                return 0, xbar,ybar
            else:
                return 0

        if view:
            vec = lambda t: (1 - t) * c1 + t * c2

            x = np.linspace(0, 1, 1000)
            y = [belongs(tf1, vec(i)) ** 2 for i in x]
            z = [belongs(tf2, vec(i)) ** 2 for i in x]
            plt.plot(x, y, color='b')
            plt.plot(x, z, color='r')
            plt.axvline(t1, ls='--', color='b')
            plt.axvline(t2, ls='--', color='r')
            plt.ylim(0, chi2.ppf(alpha, 3))
            plt.show()

        if view:
            pc = PointCloud([c1, c2, xbar, ybar])
            actors = tf1.get_actor() + tf2.get_actor() + pc.get_actor(color='green', glyph='sphere', scale=0.05) + [segment_primitive(c1, c2, color='#808080', lw=1)]

        theta1 = angle(ybar - xbar, quadratic(tf1, xbar))
        theta2 = angle(xbar - ybar, quadratic(tf2, ybar))

        if theta1<epsilon and theta2<epsilon:
            #print('\n')
            if output:
                return norm(xbar-ybar),xbar,ybar
            else:
                return norm(xbar-ybar)

        gamma1 = 1 / norm(inv(
            tf1.linear @ tf1.linear.transpose()))  # < 1/max(abs(np.linalg.eigvals(inv(tf1.linear @ tf1.linear.transpose()))))
        gamma2 = 1 / norm(inv(
            tf2.linear @ tf2.linear.transpose()))  # < 1/max(abs(np.linalg.eigvals(inv(tf2.linear @ tf2.linear.transpose()))))

        c1 = xbar - gamma1 * quadratic(tf1, xbar)
        c2 = ybar - gamma2 * quadratic(tf2, ybar)

        if view:
            pcb = PointCloud([c1, c2])

            tfb1 = AffineTransform()
            tfb1.set_parameters(translation=c1, linear=np.diag([gamma1 * norm(quadratic(tf1, xbar))] * 3))
            tfb2 = AffineTransform()
            tfb2.set_parameters(translation=c2, linear=np.diag([gamma2 * norm(quadratic(tf2, ybar))] * 3))

            actors += tfb1.get_actor(color='b', alpha=chi2.cdf(1, 3)) + tfb2.get_actor(color='b', alpha=chi2.cdf(1,3)) + pcb.get_actor(color='b', glyph='sphere', scale=0.05) + [segment_primitive(c1, c2, color='#808080', lw=1)]
            display(actors)

        #sys.stdout.write('\rIteration : %i | Angles: %.6f, %.6f' % (j,theta1,theta2))
        j+=1

    # out of the loop
    if output:
        return norm(xbar - ybar), xbar, ybar
    else:
        return norm(xbar - ybar)





