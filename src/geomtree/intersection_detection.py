from itertools import combinations, product
import sys
from geomtree.affine_transform import geometric_distance

def geomdag_intersection_detection(d,iterations_max=10):
    pile = []

    for node in d.nodes():
        for ((u, tfu), (v, tfv)) in combinations(d.get_attribute(node, "children_cipher"), 2):
            tfuu = tfu.compose(d.get_attribute(u, "subdag_geometry"))
            tfvv = tfv.compose(d.get_attribute(v, "subdag_geometry"))

            if geometric_distance(tfuu, tfvv, iterations_max=iterations_max, view=False) == 0:
                pile.append((u, tfu, v, tfv))

    detected = []

    i = 1
    while len(pile) > 0:
        sys.stdout.write("\rprocessing couple n° %i / remaining size of pile %i" % (i, len(pile)))
        u, tfu, v, tfv = pile.pop()
        __geomdag_couple_intersection(d, u, tfu, v, tfv, detected, pile,iterations_max)
        i += 1

    return detected


def __geomdag_couple_intersection(d, u, tfu, v, tfv, detected, pile,iterations_max):
    # comparing roots

    tfuu = tfu.compose(d.get_attribute(u, "geometry"))
    tfvv = tfv.compose(d.get_attribute(v, "geometry"))

    if geometric_distance(tfu.compose(d.get_attribute(u, "geometry")), tfv.compose(d.get_attribute(v, "geometry")),
                          iterations_max=iterations_max, view=False) == 0:
        detected.append((u, tfu, v, tfv))

    # comparing subtrees

    tfuu = tfu.compose(d.get_attribute(u, "subdag_geometry"))
    tfvv = tfv.compose(d.get_attribute(v, "subdag_geometry"))

    if tfvv.volume() < tfuu.volume():
        u, tfuu, v, tfvv = v, tfvv, u, tfuu

    for cv, tfcv in d.get_attribute(v, "children_cipher"):
        tfcvv = tfv.compose(tfcv).compose(d.get_attribute(cv, "subdag_geometry"))
        if geometric_distance(tfuu, tfcvv, iterations_max=iterations_max, view=False) == 0:
            pile.append((u, tfu, cv, tfv.compose(tfcv)))

# ------------------------------------------------#
def geomtree_intersection_detection(gt,iterations_max=10):
    pile = []

    __self_intersection(gt, pile,iterations_max)

    detected = []

    i = 1
    while len(pile) > 0:
        sys.stdout.write("\rprocessing couple n° %i / remaining size of pile %i" % (i, len(pile)))
        u, v = pile.pop()
        __couple_intersection(u, v, detected, pile,iterations_max)
        i += 1

    return detected

def __self_intersection(gt, pile,iterations_max):
    for u, v in combinations(gt.my_children, 2):

        tfu = u.get_attribute('subtree_geometry')
        tfv = v.get_attribute('subtree_geometry')

        if geometric_distance(tfu, tfv, iterations_max=iterations_max, view=False) == 0:
            pile.append((u, v))

    for child in gt.my_children:
        __self_intersection(child, pile,iterations_max)


def __couple_intersection(u, v, detected, pile,iterations_max):
    # comparing roots

    if geometric_distance(u.my_geometry, v.my_geometry, iterations_max=iterations_max, view=False) == 0:
        detected.append((u.my_id, v.my_id))

    # comparing subtrees

    if v.get_attribute('subtree_geometry').volume() < u.get_attribute('subtree_geometry').volume():
        u, v = v, u

    for cv in v.my_children:
        if geometric_distance(u.get_attribute('subtree_geometry'), cv.get_attribute('subtree_geometry'),
                              iterations_max=iterations_max, view=False) == 0:
            pile.append((u, cv))