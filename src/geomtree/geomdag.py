from treex.ciphering_dag import *
from geomtree.affine_transform import *
from geomtree.geomtree import GeomTree

def affine_transform_to_tuple(tf):
    return tuple(list(tf.translation) + list(tf.linear.flatten()))


def tuple_to_affine_transform(tup):
    translation = np.array(tup[:3])
    linear = np.array(tup[3:]).reshape(3, 3)
    tf = AffineTransform()
    tf.set_parameters(translation, linear)
    return tf

def add_geometry_as_label(gt, labels):
    gt.add_attribute_to_id(labels, affine_transform_to_tuple(gt.my_geometry))
    for child in gt.my_children:
        add_geometry_as_label(child, labels)

# ------------------------------------------------#

def kabsch_algorithm(f, objective=None):
    # P sur Q !!!!

    # centres uniquement pour le moment
    p = np.array([tuple_to_affine_transform(k).translation for k in f.keys()])
    q = np.array([tuple_to_affine_transform(f[k]).translation for k in f.keys()])

    cp = np.mean(p, axis=0)
    cq = np.mean(q, axis=0)
    p = p - cp
    q = q - cq

    U, _, V = svd(p.transpose() @ q)
    R = U @ np.diag([1, 1, np.sign(det(U @ V))]) @ V
    q = np.transpose(R @ q.transpose())

    tf = AffineTransform()
    tf.set_parameters(linear=R, translation=cp - R @ cq)

    return tf


def alignment_error(f, objective, param):
    from geomtree.point_cloud import sphere_surface_grid

    # P sur Q !!!!

    p1 = np.empty(shape=[0, 3])
    p2 = np.empty(shape=[0, 3])
    q1 = np.empty(shape=[0, 3])
    q2 = np.empty(shape=[0, 3])

    pts = sphere_surface_grid(100)

    for k in f.keys():
        # centres uniquement pour le moment

        tfp = tuple_to_affine_transform(k)
        tfq = tuple_to_affine_transform(f[k])

        p1 = np.concatenate((p1, param.apply_transform(tfp.translation.reshape(-1, 3))))
        q1 = np.concatenate((q1, tfq.translation.reshape(-1, 3)))

        # ellipsoid hack

        pb = param.apply_transform(tfp.apply_transform(pts))
        qb = tfq.apply_transform(pts)

        p2 = np.concatenate((p2, pb[np.argsort(np.linalg.norm(pb, axis=1))]))
        q2 = np.concatenate((q2, qb[np.argsort(np.linalg.norm(qb, axis=1))]))

    p = np.concatenate((p1, p2))
    q = np.concatenate((q1, q2))

    return np.linalg.norm(p - q) / np.sqrt(len(p))

# ------------------------------------------------#

def geomtree_to_geomdag(gt, epsilon=float('Inf')):
    labels = 'geometry'
    objective = None
    estimate = kabsch_algorithm
    error = alignment_error

    add_geometry_as_label(gt, labels)
    return tree_to_ciphering_dag(gt, labels, False, epsilon=epsilon, objective=objective, estimate=estimate,
                                 error=error)

def __reconstruct(d, node, tf, labels):
    gt = GeomTree(tf.compose(tuple_to_affine_transform(d.get_attribute(node, labels))))
    gt.add_attribute_to_id(labels, affine_transform_to_tuple(gt.my_geometry))
    for child, ctf in d.get_attribute(node, 'children_cipher'):
        gt.add_subtree(__reconstruct(d, child, tf.compose(ctf), labels))
    return gt

def geomdag_to_geomtree(d):
    return __reconstruct(d,d.my_roots[0],AffineTransform(),'geometry')


def compute_subdag_geometry(d, sphere_pts=100, tolerance=1e-3):
    from geomtree.point_cloud import PointCloud, sphere_surface_grid
    sphere_pts = sphere_surface_grid(sphere_pts)
    nodes = d.nodes_sorted_by("height")
    for h in sorted(nodes.keys()):
        for node in nodes[h]:
            tf = tuple_to_affine_transform(d.get_attribute(node, 'geometry'))
            d.add_attribute_to_node('geometry', tf, node)

            pts = tf.apply_transform(sphere_pts)
            for child, ctf in d.get_attribute(node, 'children_cipher'):
                tf = ctf.compose(d.get_attribute(child, 'subdag_geometry'))
                pts = np.concatenate((pts, tf.apply_transform(sphere_pts)), axis=0)
            if len(d.get_attribute(node, 'children_cipher')) > 0:
                tf = PointCloud(pts).minimal_volume_enclosing_ellipsoid(tolerance)
            d.add_attribute_to_node("subdag_geometry", tf, node)
