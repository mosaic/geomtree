"""
A package for treating trees augmented with 3D geometry
"""
# {# pkglts, src
# FYEO
# #}
# {# pkglts, version, after src
from . import version

__version__ = version.__version__
# #}

from geomtree.affine_transform import *
from geomtree.point_cloud import *
from geomtree.geomtree import *
from geomtree.geomdag import *
from geomtree.intersection_detection import *