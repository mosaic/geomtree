# coding=utf-8
from treex import Tree
from treex.simulation import gen_random_tree
from geomtree.affine_transform import AffineTransform, random_transform, hellinger_distance, geometric_distance

import numpy as np
from scipy.linalg import svd, det

class GeomTree(Tree):

    __slots__ = 'my_geometry'

    def __init__(self,tf=None):
        super().__init__()
        if tf is not None:
            self.my_geometry = tf
        else:
            self.my_geometry = AffineTransform()

    def get_actor(self,axis=False,alpha=0.95,opacity=0.2,show_ellipsoid=True):
        actors = []
        self.__get_recursive_actor(actors,axis,alpha,opacity,show_ellipsoid)
        return actors

    def __get_recursive_actor(self,actors,axis,alpha,opacity,show_ellipsoid):
        from visu_core.vtk.primitive_actors import segment_primitive, point_cloud_primitive

        if 'actor_color' in self.get_attribute():
            color=self.get_attribute('actor_color')
        else:
            color='glasbey_'+str(self.get_property('depth'))
        if show_ellipsoid:
            actors+=self.my_geometry.get_actor(color=color,axis=axis,alpha=alpha,opacity=opacity)
        else:
            actors+=[point_cloud_primitive([self.my_geometry.translation], glyph='sphere', scale=0.1, color=color)]
        c = self.my_geometry.translation
        for child in self.my_children:
            d = child.my_geometry.translation
            actors+=[segment_primitive(c,d,color='#808080',lw=1)] ### epaisseur du segment ! #TODO: gerer par un argument l'epaisseur
            child.__get_recursive_actor(actors,axis,alpha,opacity,show_ellipsoid)

    def apply_tf(self,tf):
        self.my_geometry=tf.compose(self.my_geometry)
        for child in self.my_children:
            child.apply_tf(tf,relative)

    def get_geometry(self):
        geometry = {self.my_id : self.my_geometry}
        for child in self.my_children:
            geometry.update(child.get_geometry())
        return geometry

    def compute_subtree_geometry(self,sphere_pts=100,tolerance=1e-3):
        from geomtree.point_cloud import PointCloud, sphere_surface_grid
        if isinstance(sphere_pts,int):
            sphere_pts = sphere_surface_grid(sphere_pts)

        tf = self.my_geometry
        pts=tf.apply_transform(sphere_pts)

        for child in self.my_children:
            if 'subtree_geometry' not in child.get_attribute():
                child.compute_subtree_geometry(sphere_pts,tolerance)

            tf = child.get_attribute('subtree_geometry')
            pts = np.concatenate((pts, tf.apply_transform(sphere_pts)),axis=0)

        if len(self.my_children)>0:
            tf =PointCloud(pts).minimal_volume_enclosing_ellipsoid(tolerance)
            self.add_attribute_to_id("subtree_geometry",tf)
        else:
            # tree is a leaf
            self.add_attribute_to_id("subtree_geometry",tf.copy())


def align_centers(t1,t2,tree_type="unordered", rmsd=True):
    """
    If the two trees are isomorphic, find the best alignment of centers of the two trees via Kabsch algorithm
    """
    bool, mapp = t1.is_isomorphic_to(t2, tree_type,mapping=True)

    assert bool, "Trees are not isomorphic"

    geom1 = t1.get_geometry()
    geom2 = t2.get_geometry()

    p=np.array([geom1[k].translation for k in mapp.keys()])
    q=np.array([geom2[mapp[k]].translation for k in mapp.keys()])

    if rmsd:
        print("RMSD before processing : ", np.linalg.norm(p - q)/np.sqrt(len(p)))

    cp = t1.my_geometry.translation
    cq = t2.my_geometry.translation
    p = p - cp
    q = q - cq

    if rmsd:
        print("RMSD after root alignment : ", np.linalg.norm(p - q)/np.sqrt(len(p)))

    U, _, V = svd(p.transpose() @ q)
    R = U @ np.diag([1, 1, np.sign(det(U @ V))]) @ V
    q = np.transpose(R @ q.transpose())

    if rmsd:
        print("RMSD after optimal rotation : ", np.linalg.norm(p - q)/np.sqrt(len(p)))

    tf = AffineTransform()
    tf.set_parameters(linear=R, translation=cp - R @ cq)

    #return tf.inverse(), np.linalg.norm(p - q)/np.sqrt(len(p))
    return tf, np.linalg.norm(p - q)/np.sqrt(len(p))

def tree_hellinger_distance(t1,t2,tree_type="unordered"):

    bool, mapp = t1.is_isomorphic_to(t2, tree_type,mapping=True)

    assert bool, "Trees are not isomorphic"

    geomu = t1.get_geometry()
    geomv = t2.get_geometry()

    p=np.array([geomu[k] for k in mapp.keys()])
    q=np.array([geomv[mapp[k]] for k in mapp.keys()])
    f = 0
    for i in range(len(p)):
        f += hellinger_distance(p[i], q[i])
    return f/t1.get_property('size') #pour rester dans [0,1]


def tree_distance(t1, t2, isom):
    geomu = t1.get_geometry()
    geomv = t2.get_geometry()

    p = np.array([geomu[k] for k in isom.keys()])
    q = np.array([geomv[isom[k]] for k in isom.keys()])
    f = 0
    for i in range(len(p)):
        f += geometric_distance(p[i], q[i], iterations_max=100, view=False)
    return f

def tree_to_geomtree(t):
    gt = GeomTree(random_transform())
    for child in t.my_children:
        gc = tree_to_geomtree(child)
        gt.add_subtree(gc)
    return gt

# tf=random_transform()
#
# pc1 = tf.sample_points(n=1000,method='uniform')
# pc2 = tf.sample_points(n=1000)
#
# display(tf.get_actor(opacity=0.1)+pc2.get_actor(glyph='sphere',scale=0.1))
# display(tf.get_actor(opacity=0.1,alpha=chi2.cdf(1,3))+pc1.get_actor(glyph='sphere',scale=0.1))