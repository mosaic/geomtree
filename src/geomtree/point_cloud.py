import numpy as np

from scipy.spatial.transform import Rotation as R
from scipy.linalg import svd, inv, norm
from scipy.stats import multivariate_normal, chi2
from scipy.spatial.distance import mahalanobis
from sklearn.covariance import MinCovDet

class PointCloud(object):
    """
    This class implements point clouds.

    :ivar cloud: (n,3) array
    :ivar skinning_vector: dict of float
    """

    # TODO : skinning

    __slots__ = 'cloud'#, 'skinning_vector'

    def __init__(self,points):
        """
        :param points: the actual cloud
        """
        self.cloud = points #: Stack of vectors :math:`X_1,\dots,X_n`.
        #self.skinning_vector = {} #:not implemented yet

    def get_actor(self,color=None,filter=None,glyph='point',scale=1):
        from visu_core.vtk.primitive_actors import point_cloud_primitive
        # points

        if filter is None:
            cloud = {0: np.array(self.cloud)}
            if color is None:
                color_list = {0: 'r'}
            else:
                color_list = {0: color}
            glyph_list= {0 : glyph}

        else:
            cloud = self.subdivise_cloud(filter)
            if color is None:
                color_list={}
                i=0
                for val in cloud.keys():
                    color_list[val]='glasbey_'+str(i)
                    i+=1
            else:
                assert set(cloud.keys()).issubset(set(color.keys())), "Colors provided should match the values in filter"
                color_list = color
            if isinstance(glyph,dict):
                assert set(cloud.keys()).issubset(set(glyph.keys())), "Glyphs provided should match the values in filter"
                glyph_list = glyph
            else:
                glyph_list={}
                for val in cloud.keys():
                    glyph_list[val]=glyph

        actors=[]
        for k in cloud.keys():
            actors.append(point_cloud_primitive(cloud[k], glyph=glyph_list[k], scale=scale, color=color_list[k]))
        return actors

    # --------------------------------------------------#
    # AffineTransform Estimators

    def estimate_gaussian(self):
        """
        Estimate of the gaussian distribution (as an affine transformation of the normal law) from which the points have been generated.
        Translation part :math:`t` is estimated as the barycenter of the points,
        whereas the linear transformation part :math:`T` is defined from the Singular Value Decomposition (SVD) of the covariance matrix :math:`\\Sigma`.
        In details, :math:`\\Sigma = US^2V^{\\top}` and :math:`T=US`.

        :return: AffineTransform
        """
        from geomtree.affine_transform import AffineTransform
        translation = np.mean(self.cloud,0)
        cov_mat = np.cov(self.cloud.T)
        u, s, v = svd(cov_mat)
        scale= np.diag(np.sqrt(s))
        tf = AffineTransform()
        tf.set_parameters(translation, u @ scale) #TODO: u or v ?
        return tf

    # def estimate_mincovdet(self):
    #     # not very useful
    #     from geomtree.affine_transform import AffineTransform
    #     cov = MinCovDet().fit(self.cloud)
    #     cov_mat = cov.covariance_
    #     translation = cov.location_
    #     u, s, v = svd(cov_mat)
    #     scale= np.diag(np.sqrt(s))
    #     tf = AffineTransform()
    #     tf.set_parameters(translation, u @ scale) #TODO: u or v ?
    #     return tf

    def minimal_volume_enclosing_ellipsoid(self,tolerance=0.01):
        #TODO : meilleur algo??
        """
        Find the minimum volume ellipsoid which holds all the points of the cloud.

        Adaptation of the work by Michael Imelfort (https://github.com/minillinim/ellipsoid/blob/master/ellipsoid.py), itself based on work by Nima Moshtagh (http://www.mathworks.com/matlabcentral/fileexchange/9542).

        To respect scaling, the obtained ellipsoid should be plotted with ``alpha=chi2.cdf(1,3)``.

        :param tolerance: float, optional (default=0.01)
        :return: AffineTransform
        """
        from geomtree.affine_transform import AffineTransform
        N= len(self.cloud)

        # Q will be our working array
        Q = np.vstack([np.copy(self.cloud.T), np.ones(N)])
        QT = Q.T

        # initializations
        err = 1.0 + tolerance
        u = (1.0 / N) * np.ones(N)

        # Khachiyan Algorithm
        while err > tolerance:
            V = Q @ np.diag(u) @ QT
            M = np.diag(QT @ inv(V) @ Q)  # M the diagonal vector of an NxN matrix
            j = np.argmax(M)
            maximum = M[j]
            step_size = (maximum - 4.0) / ((4.0) * (maximum - 1.0))
            new_u = (1.0 - step_size) * u
            new_u[j] += step_size
            err = norm(new_u - u)
            u = new_u

        tf = AffineTransform()

        # center of the ellipse
        center = self.cloud.T @ u

        # the A matrix for the ellipse
        A = inv(self.cloud.T @ np.diag(u) @ self.cloud -np.array([[a * b for b in center] for a in center])) / 3.0

        U, s, _ = svd(A) #TODO: U or V ?
        S = np.diag(1.0 / np.sqrt(s)) #TODO: renormalize for checking alpha/radius stuff

        tf.set_parameters(translation=center,linear= U @ S)
        return tf

    #--------------------------------------------------#
    # Filters

    def subdivise_cloud(self,filter):
        """
        Filter the point cloud :math:`X_1,\dots, X_n`` according to a list of length :math:`n`.

        A dictionary is created, each key ``k`` being a unique value of ``filter``, and values being a stack of the points
        :math:`X_i` such that ``filter[i-1]=k``.

        :param filter: list
        :return: dict
        """
        assert len(filter)==len(self.cloud), "Filter should match the number of points"
        values = np.unique(filter)
        sub_cloud = {}
        for val in values:
            sub_cloud[val]=np.array([self.cloud[i] for i in range(len(filter)) if filter[i] == val])
        return sub_cloud

    def mahalanobis_distance_filter(self,tf,alpha=0.95,r=None):
        """
        Returns a vector of boolean, checking for each point of the cloud if it belong to the ellipsoid induced by ``tf``
        and whose scale is set to contain ``alpha`` % of the density of the gaussian distribution induced by ``tf``.

        Alternatively, a radius ``r`` may also be provided instead of ``alpha``; in this case, the function tests if the Mahalanobis distance between each point and ``tf`` fall under that radius or not.

        TODO: proper references to the math behind it

        :param tf: AffineTransform
        :param alpha: float
        :param r: float
        :return: list of boolean
        """
        if r is not None:
            qr = r
        else:
            qr = np.sqrt(chi2.ppf(alpha, 3))
        return [mahalanobis(vec, tf.translation, inv(tf.get_covariance_matrix())) <= qr for vec in self.cloud]

    def __get_likelihood(self,tf):
        mu = tf.translation
        sigma = tf.get_covariance_matrix()
        return multivariate_normal.pdf(self.cloud,mu,sigma)

    def intersection_filter(self,tf1,tf2,evidence=10):
        """
        Let :math:`(T_1,t_1)` and :math:`(T_2,t_2)` be two affine transformations, and :math:`N_1` and :math:`N_2` their respective induced Gaussian distributions.

        Using Bayes Factor [1], this function determines the vectors :math:`X_i` from the point cloud
        for which there is evidence that they can plausibly be generated from either :math:`N_1` or :math:`N_2`.

        In details, let :math:`L^1_i = \\mathbb{P}(X_i | N_1)` and :math:`L^2_i = \\mathbb{P}(X_i | N_2)`.
        Let :math:`\\sigma` be the threshold of evidence chosen (parameter ``evidence``). According to [1], :math:`\\sigma=10` is a good option.
        Let :math:`K_i = \\frac{L^1_i}{L^2_i}`.

        If :math:`K_i>\\sigma`, there are evidence that :math:`X_i\\sim N_1`.
        If :math:`K_i<1/\\sigma`, there are evidence that :math:`X_i\\sim N_2`.

        Otherwise, if :math:`1/\\sigma<K_i<\\sigma`, there are no evidence allowing to chose between :math:`N_1` or :math:`N_2`.

        Equivalenty, the same conclusion can be drawn if :math:`|\\log K_i |< \\log\\sigma` (1).

        This function returns a list of booleans for whether each :math:`X_i` fulfill or not the condition (1).

        Seeing :math:`N_1` and :math:`N_2` as ellipsoids, those :math:`X_i` for which (1) is fulfilled can be seen as the points at the intersection of both ellipsoids.

        :param tf1: AffineTransform
        :param tf2: AffineTransform
        :param evidence: float, optional (default is 10)
        :return: list of booleans

        References
        ----------
        [1] Kass, R. E., & Raftery, A. E. (1995). Bayes factors. Journal of the american statistical association, 90(430), 773-795.
        """
        # check if the absolute value of the log10 of bayes factor is inferior to the log10 of the evidence
        l1  =self.__get_likelihood(tf1)
        l2 = self.__get_likelihood(tf2)
        return abs(np.log10(l1/l2)) < np.log10(evidence)

    def intersection_filter_bis(self,tf1,tf2,evidence=10):
        K = 2 * np.log(evidence) #TODO: vérifier que 2 est correct
        filter=[]
        for vec in self.cloud:
            m1 = mahalanobis(vec, tf1.translation, inv(tf1.get_covariance_matrix()))**2
            m2 = mahalanobis(vec, tf2.translation, inv(tf2.get_covariance_matrix()))**2
            filter.append(abs(m1-m2)<K)
        return filter

def sphere_surface_grid(n):
    """
    Sample evenly n points on the surface of the unit 3D-sphere.
    :return: PointCloud
    """
    # taken from http://extremelearning.com.au/how-to-evenly-distribute-points-on-a-sphere-more-effectively-than-the-canonical-fibonacci-lattice/
    goldenRatio = (1 + 5**0.5)/2
    i = np.arange(0, n)
    theta = 2 *np.pi * i / goldenRatio
    phi = np.arccos(1 - 2*(i+0.5)/n)
    x, y, z = np.cos(theta) * np.sin(phi), np.sin(theta) * np.sin(phi), np.cos(phi)
    return np.concatenate((np.eye(3),-np.eye(3),np.concatenate((x.reshape(-1,1),y.reshape(-1,1),z.reshape(-1,1)),axis=1)))

    # pts = PointCloud(sphere_surface_grid(100))
    # tf = AffineTransform()
    #
    # display(tf.get_actor(alpha=chi2.cdf(1, 3)) + pts.get_actor(glyph='sphere', scale=0.1))
    #
    # tf = random_transform()
    # pt = PointCloud(tf.apply_transform(pts))
    #
    # display(tf.get_actor(alpha=chi2.cdf(1, 3)) + pt.get_actor(glyph='sphere', scale=0.1))